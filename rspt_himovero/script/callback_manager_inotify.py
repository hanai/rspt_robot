# encoding: utf-8

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__version__ = "0.0.1"
__date__ = "17 May 2018"

import pyinotify
from callback_manager import *


class EventHandler(pyinotify.ProcessEvent):
    def __init__(self, watch_file, handler):
        self._watch_file = watch_file
        self.handler = handler
        super(EventHandler, self).__init__()

    def process_IN_MODIFY(self, event):
        """
        ファイルの更新日時変更であるIN_MODIFYイベントの発生はファイルの書込み完了を意味しない。
        複数の書込みが行われる場合、書込み完了前にイベントが発生したり、複数回発生する可能性がある。
        これを回避するため、書込みでオープンされたファイルがクローズされたことを示すイベントである
        CLOSE_WRITEを用いている。
        また、書込み前にinode情報を変更するファイルシステムもあるらしい。
        """

    def process_IN_CLOSE_WRITE(self, event):
        print(event)
        print("Close Write:", event.pathname)
        if event.pathname == self._watch_file:
            self.handler()

class HiMoveROCallbackManagerINotify(HiMoveROCallbackManager):
    """
    HiMoveROがイベント通知用のファイルを更新すると、その内容にしたがって
    NextageOPENの指定した関数を実行するスクリプト
    ただし、inotifyを使ったバージョンはwatchするファイルがローカルにある場合のみ動作する。
    NFS等でマウントした場合、ファイル更新イベントがカーネルを介さないため、inotifyでは検出できない。

    $ sudo apt install python-pyinotify
    """

    def __init__(self, watch_file):
        super(HiMoveROCallbackManagerINotify, self).__init__(watch_file)

        wm = pyinotify.WatchManager()
        self.handler = EventHandler(self.watch_file(), self.process_msg)
        self.notifier = pyinotify.Notifier(wm, self.handler)
        wdd = wm.add_watch(self.watch_file(), pyinotify.IN_MODIFY | pyinotify.IN_CLOSE_WRITE, rec=True)

    def loop(self):
        self.notifier.loop() # Ctrl+c で終了
