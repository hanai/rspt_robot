#!/usr/bin/python
# encoding: utf-8

import time
import pyinotify


class Handler(pyinotify.ProcessEvent):

    def process_IN_CREATE(self, event):
        # ファイルやディレクトリが追加された時に呼ばれる
        print 'Create :'
        print event

    def process_IN_DELETE(self, event):
        # ファイルやディレクトリが削除された時に呼ばれる
        print 'Delete :'
        print event

    def process_default(self, event):
        # それ以外の場合に呼ばれる
        print event.maskname
        print event

wm = pyinotify.WatchManager()

# 監視スレッドを作成し、走らせる。
notifier = pyinotify.ThreadedNotifier(wm, Handler())
notifier.start()

# 監視するイベントの種類
mask = pyinotify.IN_CREATE | pyinotify.IN_DELETE | pyinotify.IN_MODIFY

# 監視対象の追加
wdd = wm.add_watch('/tmp/hoge.txt', mask)

time.sleep(30)

# 監視対象の削除
#  <b>wdd['/tmp']</b>みたいにすれば特定のディレクトリだけ削除できる。
#  ちなみに、python3.xの場合は<b>list(wdd.values())</b>としなければいけないみたい。
wm.rm_watch(wdd.values())

# 監視スレッドの終了
#  これを忘れるとプログラムが終了しません。
#  そのまえで例外で終了しちゃったりすると、killするしかなくなるみたい。
notifier.stop()
