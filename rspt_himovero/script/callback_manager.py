# encoding: utf-8

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__version__ = "0.0.1"
__date__ = "17 May 2018"

import os

"""
RobotWorking.txtのフォーマット

WorkingNo=0
Status=0
CountNum=0
CameraWorkingNo=0
CameraView=0,0,0,0,0,0
CameraBase=0,0,0,0,0,0
CameraBasePosition=0,0,0
FILEEND
"""

def read_robotworking_file(filename):
    ret = {}

    with open(filename, 'r') as f:
        for ln in [line.strip() for line in f.readlines()]:
            if ln == 'FILEEND':
                break
            var,val = ln.split('=')
            if var == 'WorkingNo' or var == 'Status' or var == 'CountNum':
                ret[var] = int(val)
            elif var == 'CameraWorkingNo' or var == 'CameraView' or var == 'CameraBase' or var == 'CameraBasePosition':
                pass # ignore these tags
            else:
                print('unknown tag: %s'% var)
    return ret

def write_robotworking_file(filename, msg):
    data = ('WorkingNo=%d\n'
            'Status=%d\n'
            'CountNum=%d\n'
            'CameraWorkingNo=0\n'
            'CameraView=0,0,0,0,0,0\n'
            'CameraBase=0,0,0,0,0,0\n'
            'CameraBasePosition=0,0,0\n'
            'FILEEND\n'
            %(msg['WorkingNo'], msg['Status'], msg['CountNum']))

    with open(filename, 'w') as f:
        f.write(data)


class HiMoveROCallbackManager(object):
    def __init__(self, watch_file='/HPM/Active/RobotWorking.txt'):
        if not os.path.exists(watch_file):
            print('create "%s"' % watch_file)
            write_robotworking_file(watch_file, {'WorkingNo':0, 'Status':0, 'CountNum':0})
        self._watch_file = watch_file
        self.callbacks = {}

    def watch_file(self):
        return self._watch_file

    def process_msg(self):
        msg = read_robotworking_file(self.watch_file())
        print(msg)
        if msg['Status'] == 1:
            try:
                for _ in range(msg['CountNum']):
                    self.callbacks[msg['WorkingNo']]()
                msg['Status'] = 0
                write_robotworking_file(self.watch_file(), msg)
            except KeyError as ke:
                print('Callback is not registered for WorkingNo=%d' % msg['WorkingNo'])
        else:
            #print('Status is set to 0 by another program (unexpected)')
            pass # ignore the event caused by myself (Status <- 0)

    def add_callback(self, working_no, callback):
        self.callbacks[working_no] = callback

    def print_callbacks(self):
        print(self.handler.callbacks)
