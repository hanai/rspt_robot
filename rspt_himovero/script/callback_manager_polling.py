# encoding: utf-8

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__version__ = "0.0.1"
__date__ = "15 May 2018"

import os, time
from callback_manager import *


class HiMoveROCallbackManagerPolling(HiMoveROCallbackManager):
    """
    HiMoveROがイベント通知用のファイルを更新すると、その内容にしたがって
    NextageOPENの指定した関数を実行するスクリプトのpolling版。
    NFS等でマウントしたリモートファイルにも利用できるが、処理は重い。

    （注意）
    ファイルの更新日時が変更されるタイミングで書込みが完了している保証はない。
    ファイルの中身を見てStatus=1を確認したとしても、そのタイミングでファイルの他の場所の書込みが
    完了している保証が無い点に注意が必要である。
    tmpファイルにwriteしてからrename（Posixによるとrename()はatomic）するなどが考えられるが、
    リモートのファイルの場合にこの方法が利用できるかどうかは調査が必要である。

    これは、HiMoveRO側のプログラムにも変更が必要なので、方針が定まっていない現段階では実装しない。
    ポーリングの頻度が低ければ、書込みの非アトミック性が問題になる確率は小さい。
    readとwriteの間に割り込まれる可能性については、両者がStatusを1=>0, 0=>1という一方向にしか
    変更しない場合に限り問題にならないと考えられる。
    """

    def __init__(self, watch_file):
        super(HiMoveROCallbackManagerPolling, self).__init__(watch_file)
        self._last_mtime = self.time_stamp()

    def time_stamp(self):
        return os.stat(self.watch_file()).st_mtime

    def set_polling_interval(self, sec):
        self._interval_sec = sec

    def loop(self):
        while True:
            stamp = self.time_stamp()
            if stamp != self._last_mtime:
                self._last_mtime = stamp
                print("Modify:", self.watch_file())
                self.process_msg()
            time.sleep(self._interval_sec)
