#!/usr/bin/python
# encoding: utf-8

#from callback_manager_inotify import *
from callback_manager_polling import *


from nextage_ros_bridge import nextage_client
robot = nextage_client.NextageClient()

# from hrpsys import rtm
# import argparse

sim = True

if sim == True: # for simulation
    robot.init(robotname='HiroNX(Robot)0', url='')
else: # for real robot
    # rtm.nshost = args.host
    # rtm.nsport = args.port
    robot.init(robotname='RobotHardware0', url='/opt/jsk/etc/NEXTAGE/model/main.wrl')


"""
HiMoveROからのイベント通知時に実行する関数を定義する。
"""

def rhand_up():
    print('rhand_up:')
#    robot.setTargetPoseRelative('rarm', 'RARM_JOINT5', dz=0.1, wait=True)

def rhand_down():
    print('rhand_down:')
    # xyz = robot.getCurrentPosition('RARM_JOINT5')
    # rpy = robot.getCurrentRPY('RARM_JOINT5')
    # tm = 2.0
    # xyz[2] -= 0.1
    # robot.setTargetPose('rarm', xyz, rpy, tm)
    # robot.waitInterpolationOfGroup('rarm')

def rgripper_open():
    print('rgripper_open:')
    # robot._hands.gripper_r_open()

def rgripper_close():
    print('rgripper_close:')
    # robot._hands.gripper_r_close()


"""
定義した関数を登録する。
第一引数の番号は、HiMoveROの管理プログラムにおけるWorkingNoに対応する。
"""

#cbm = HiMoveROCallbackManagerINotify(watch_file='/tmp/RobotWorking.txt')
cbm = HiMoveROCallbackManagerPolling(watch_file='/tmp/RobotWorking.txt')
cbm.add_callback(1, rhand_up)
cbm.add_callback(2, rhand_down)
cbm.add_callback(3, rgripper_open)
cbm.add_callback(4, rgripper_close)

if __name__ == '__main__':
    cbm.loop() # Ctrl+cで終了する。
