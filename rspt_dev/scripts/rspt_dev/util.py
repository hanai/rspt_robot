import numpy as np
import tf.transformations as t
from colorama import Fore
import geometry_msgs.msg

def message(msg, color='g'):
    if color == 'g':
        print(Fore.GREEN + msg + Fore.RESET)
    elif color == 'b':
        print(Fore.LIGHTBLUE_EX + msg + Fore.RESET)
    elif color == 'c':
        print(Fore.LIGHTCYAN_EX + msg + Fore.RESET)
    elif color == 'm':
        print(Fore.LIGHTMAGENTA_EX + msg + Fore.RESET)
def warn(msg):
    print(Fore.LIGHTYELLOW_EX + msg + Fore.RESET)
def error(msg):
    print(Fore.LIGHTRED_EX + msg + Fore.RESET)

def interp(x, y, r):
    # print(x,y,r)
    return (1. - r) * x + r * y

def pose2mat(p):
    """
    convert ROS Pose to transform matrix or arrays expressing (position, quaternion)
    """
    if isinstance(p, geometry_msgs.msg.Pose):
        a = p.position
        b = p.orientation
        position = [a.x,a.y,a.z]
        orientation = [b.x,b.y,b.z,b.w]
    else:
        position,orientation = p
    return np.dot(t.translation_matrix(position), t.quaternion_matrix(orientation))
    #return np.dot(t.quaternion_matrix(orientation), t.translation_matrix(position))


def mat2pose(m, o_unit='n'):
    """
    Convert a transform matrix into a (vector, quaternion) pair.

    Args:
        m: matrix
        o_unit: Output format. 'n' for np.array; 'p' for ROS Pose.

    Returns:
       a pair of (vector, quaternion)
    """
    p = geometry_msgs.msg.Pose()
    x,y,z = t.translation_from_matrix(m)
    p.position.x = x
    p.position.y = y
    p.position.z = z
    qx,qy,qz,qw = t.quaternion_from_matrix(m)
    p.orientation.x = qx
    p.orientation.y = qy
    p.orientation.z = qz
    p.orientation.w = qw
    return p

def rotate_vector(q, v):
    vp = np.append(v, 0)
    return t.quaternion_multiply(t.quaternion_multiply(q, vp), t.quaternion_conjugate(q))[:3]


from rospkg import RosPack
import kdl_parser_py.urdf
import PyKDL as kdl

#setup KDL
class IKSolver:
    def __init__(self):
        rp = RosPack()
        filename = rp.get_path('ur3dual_description') + '/urdf/ur3dual_robot.urdf'
        (ok, tree) = kdl_parser_py.urdf.treeFromFile(filename)
        if ok:
            print('kdl: urdf parse, succeeded')
        else:
            print('kdl: urdf parse, failed')
            return
        self.chain = tree.getChain('world', 'rarm_wrist_3_link')
        self.fksolvervel = kdl.ChainFkSolverVel_recursive(self.chain)
        self.fksolverpos = kdl.ChainFkSolverPos_recursive(self.chain)
        self.iksolvervel = kdl.ChainIkSolverVel_pinv(self.chain)
        self.iksolverpos = kdl.ChainIkSolverPos_NR(self.chain,self.fksolverpos,self.iksolvervel)
        self.q_zero = kdl.JntArray(self.chain.getNrOfJoints())
        self.q_solved = kdl.JntArray(self.chain.getNrOfJoints())
        self.E_NOERROR = 0 # defined in solveri.hpp

    def solve(self, q_init, frame):
        for i in range(self.q_zero.rows()):
            self.q_zero[i] = q_init[i]
        p,q = frame
        F = kdl.Frame(kdl.Rotation.Quaternion(*q), kdl.Vector(*p))
        ret = self.iksolverpos.CartToJnt(self.q_zero, F, self.q_solved)
        if ret == self.E_NOERROR:
            return True
        else:
            error('IK failed: ', ret)
            return False

    def get_last_solution(self):
        n = self.q_solved.rows()
        q_ret = np.zeros(n)
        for i in range(n):
            q_ret[i] = self.q_solved[i]
        return q_ret
