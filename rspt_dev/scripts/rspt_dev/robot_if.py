#!/usr/bin/env python

from util import *
import rospy
import moveit_commander

import sys
import actionlib
import robotiq_msgs.msg

import time
import math
import numpy as np
import tf
import tf2_ros

from geometry_msgs.msg import TransformStamped, Pose, WrenchStamped, TwistStamped
from vtac_examples.srv import set_zero
from sensor_msgs.msg import JointState


import actionlib
from control_msgs.msg import (
    FollowJointTrajectoryAction,
    FollowJointTrajectoryGoal,
    )
from trajectory_msgs.msg import (
    JointTrajectoryPoint,
    )

class Robot(object):
    def __init__(self, node_name='robot_client'):
        rospy.init_node(node_name)

        robot = moveit_commander.RobotCommander()
        print("=" * 10, " Robot Groups:")
        print(robot.get_group_names())
        print("=" * 10, " Printing robot state")
        print(robot.get_current_state())
        print("=" * 10)

        self._tf_listener = tf.TransformListener()

    def set_base_frame(self, frame):
        self._base_frame = frame

    def set_active_arm(self, arm):
        warn('set_active_arm is not yet implemented')

    def activate_admittance_control(self):
        self._admc_active = True

    def deactivate_admittance_control(self):
        self._admc_active = False

    def openg(self, timeout=5.0, force=5.0, velocity=0.1):
        self._arm.moveg(0.1, timeout, force, velocity)

    def closeg(self, timeout=5.0, force=5.0, velocity=0.1):
        self._arm.moveg(0.0, timeout, force, velocity)


    def movej(self, joint_values):
        warn('movej is not yet implemented')

    def getl(self, out_format='m'):
        '''
        return TCP pose in numpy.array format
        if out_format == 'p', returns (vec, quat)
        if out_format == 'm', return mat
        '''
        return self._arm.getl(self._base_frame, out_format=out_format)

    # def movel_RPY(self, xyz, RPY, base_frame='tcp'):
    #     quat = t.quaternion_from_euler(*RPY, axes='rxyz')
    #     self.movel(xyz, quat, base_frame=base_frame)

    def movel(self, target_frame, wait=True):
        self._arm.movel(target_frame, self._base_frame, wait)

    def movel_rel(self, target_frame, base_frame=([0,0,0],[0,0,0,1]), wait=True):
        u = target_frame[0]
        Tworld_base = pose2mat(base_frame)
        u_world = np.dot(Tworld_base[:3,:3], u)
        print('U_WORLD: ', u_world)

    # def movel_rel(self, target_frame, base_frame=([0,0,0],[0,0,0,1]), wait=True):
    #     Tbase_tcp = self.getl(base_frame)
    #     Ttcp_target = pose2mat(target_frame)
    #     Tbase_target = np.dot(Tbase_tcp, Ttcp_target)
    #     q2 = t.quaternion_from_matrix(Tbase_target)
    #     p = t.translation_from_matrix(Tbase_tcp)
    #     p2 = p + target_frame[0]
    #     self.movel((p2,q2), base_frame, wait=wait)

    # def movel_rel(self, xyzRPY = [0, 0, 0.02, 0, 0, 0]):
    #     p = self._arm.current_pose
    #     p.position.x += xyzRPY[0]
    #     p.position.y += xyzRPY[1]
    #     p.position.z += xyzRPY[2]
    #     self._arm.set_pose_target(p)
    #     self._arm.go()
        #rospy.sleep(1)

    def go_home():
        warn('go_home is not yet implemented')

    def set_frame(self, frame_id, trans, quat, parent_frame_id='world'):
        broadcaster = tf2_ros.StaticTransformBroadcaster()
        msg = TransformStamped()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = parent_frame_id
        msg.child_frame_id = frame_id

        msg.transform.translation.x = trans[0]
        msg.transform.translation.y = trans[1]
        msg.transform.translation.z = trans[2]
        msg.transform.rotation.x = quat[0]
        msg.transform.rotation.y = quat[1]
        msg.transform.rotation.z = quat[2]
        msg.transform.rotation.w = quat[3]
        broadcaster.sendTransform(msg)

    def set_frame_RPY(self, frame_id, xyz, RPY, parent_frame_id='world'):
        quat = t.quaternion_from_euler(RPY[0], RPY[1], RPY[2], axes='rxyz') # choreonoid style euler
        self.set_frame(frame_id, xyz, quat, parent_frame_id=parent_frame_id)

    def get_frame(self, frame_id, parent_frame_id='world'):
        now = rospy.Time(0)
        while not rospy.is_shutdown():
            try:
                now = rospy.Time.now()
                self._tf_listener.waitForTransform(parent_frame_id, frame_id, now, rospy.Duration(3.0))
                trans,quat = self._tf_listener.lookupTransform(parent_frame_id, frame_id, now)
                return trans,quat
            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
                rospy.logwarn(e)

    def get_frame_RPY(self, frame_id, parent_frame_id='world'):
        trans,quat = self.get_frame(parent_frame_id, frame_id)
        return trans,t.euler_from_quaternion(quat, axes='rxyz')

class UR5eHande(Robot):
    def __init__(self, node_name='robot_client'):
        super(UR5eHande, self).__init__(node_name)

        self._arm = Arm('d_bot',
                            '/gripper/gripper_action_controller',
                             'HandE',
                             '/d_bot_controller/follow_joint_trajectory',
                             '/d_bot_controller/ft_sensor/wrench_calibrated',
                             '/d_bot_controller/ft_sensor/set_zero'
                             )

        # transform from object to d_bot_wrist_3_link
        self._arm._p0 = [0, 0, 0.25]
        self._arm._q0 = t.quaternion_from_euler(-math.pi/2, math.pi/2, 0, axes='rxyz')
        # update TCP position
        # self.set_frame_RPY('d_bot_tool0', [0,0.25,0], [-1.57,0,0],
        #                        parent_frame_id='d_bot_wrist_3_link')


class Arm:
    def __init__(self, planning_group,
                     gripper_action,
                     gripper_type,
                     arm_trajectory_action,
                     wrench_topic,
                     set_zero_service
                     ):

        self._p0 = [0, 0, 0.28]
        self._q0 = t.quaternion_from_euler(-math.pi/2, -math.pi/2, 0, axes='rxyz')

        self._moveit_client = moveit_commander.MoveGroupCommander(planning_group)
        self.print_info(self._moveit_client)

        self._gripper_type = gripper_type
        self._gripper = actionlib.SimpleActionClient(gripper_action, robotiq_msgs.msg.CModelCommandAction)
        self._gripper.wait_for_server()

        self.traj_client = actionlib.SimpleActionClient(
            arm_trajectory_action,
            FollowJointTrajectoryAction
            )
        self.goal = FollowJointTrajectoryGoal()
        self.goal.goal_time_tolerance = rospy.Time(0.5)
        self.goal.trajectory.joint_names = self._moveit_client.get_joints()
        server_up = self.traj_client.wait_for_server(timeout=rospy.Duration(10.0))
        if not server_up:
            rospy.logerr("Timed out waiting for Joint Trajectory Action Server to connect.")
            return False

        rospy.Subscriber(wrench_topic, WrenchStamped, self.ft_callback, queue_size=1)
        self.set_zero_service_name = set_zero_service

    def print_info(self, moveit_client):
        print("=" * 15, "Arm ", "=" * 15)
        print("=" * 10, " Reference frame: %s" % moveit_client.get_planning_frame())
        print("=" * 10, " Reference frame: %s" % moveit_client.get_end_effector_link())

    def ft_callback(self, msg):
        # self._state = np.roll(self._state, 1, axis=1) # in-place shift would be better
        # force = msg.wrench.force
        # torque = msg.wrench.torque
        # self._state[0][0] = force.x
        # self._state[13][0] = force.y
        # self._state[14][0] = force.z
        # self._state[15][0] = torque.x
        # self._state[16][0] = torque.y
        # self._state[17][0] = torque.z

        self._state = msg
        return True

    def set_zero(self):
        rospy.wait_for_service(self.set_zero_service_name)
        set_zero_prxy = rospy.ServiceProxy(self.set_zero_service_name, set_zero)
        try:
            resp = set_zero_prxy()
        except rospy.ServiceException as e:
            print("Service did not process request: " + str(e))

    @property
    def state(self):
        return self._state

    @property
    def current_pose(self):
        return self._moveit_client.get_current_pose().pose

    def moveg(self, position=0.0, timeout=5.0, force=5.0, velocity=0.1):
        '''
        position:e goal opening in meter
        timeout: timeout in seconds
        '''
        goal = robotiq_msgs.msg.CModelCommandGoal()
        goal.force = force
        goal.velocity = velocity
        if self._gripper_type == 'HandE':
            goal.position = position/2.0
        elif self._gripper_type == 'AdaptiveGripper':
            goal.position = position
        else:
            print('unknown gripper_type: ', self._gripper_type)
            return

        try:
            #print("SEND GOAL")
            self._gripper.send_goal(goal)
            #print("WAIT FOR RESULT")
            self._gripper.wait_for_result(rospy.Duration(timeout))
            result = self._gripper.get_result()
            #print("RESULT=", result)
        except rospy.ROSInterruptException:
            rospy.loginfo("INTERRUPTED", file=sys.stderr)

    def getl(self, base_frame, out_format):
        Tbase_world = t.inverse_matrix(pose2mat(base_frame))
        Tworld_wrist = pose2mat(self.current_pose)
        Ttcp_wrist = pose2mat((self._p0, self._q0))
        Tbase_tcp = np.dot(np.dot(Tbase_world, Tworld_wrist), t.inverse_matrix(Ttcp_wrist))
        if out_format == 'p':
            return t.translation_from_matrix(Tbase_tcp), t.quaternion_from_matrix(Tbase_tcp)
        if out_format == 'm':
            return Tbase_tcp

        error('unknown out_format')
        return None

    def movel(self, target_frame, base_frame, wait):
        Tworld_base = pose2mat(base_frame)
        Tbase_tcp = pose2mat(target_frame)
        Ttcp_wrist = pose2mat((self._p0, self._q0))
        Tworld_wrist = np.dot(np.dot(Tworld_base, Tbase_tcp), Ttcp_wrist)
        p = t.translation_from_matrix(Tworld_wrist)
        q = t.quaternion_from_matrix(Tworld_wrist)

        pose = Pose()
        pose.position.x = p[0]
        pose.position.y = p[1]
        pose.position.z = p[2]
        pose.orientation.x = q[0]
        pose.orientation.y = q[1]
        pose.orientation.z = q[2]
        pose.orientation.w = q[3]

        self._moveit_client.set_pose_target(pose)
        self._moveit_client.go()


class UR3dual(Robot):
    def __init__(self, node_name='robot_client', use_moveit=True):
        super(UR3dual, self).__init__(node_name)

        self._rarm = Arm('rarm',
                             '/right_arm/gripper/gripper_action_controller',
                             'AdaptiveGripper',
                             '/right_arm/follow_joint_trajectory',
                             '/right_arm/ft_sensor/wrench_calibrated',
                             '/right_arm/ft_sensor/set_zero'
                             )
        self._larm = Arm('rarm',
                             '/left_arm/gripper/gripper_action_controller',
                             'AdaptiveGripper',
                             '/left_arm/follow_joint_trajectory',
                             '/left_arm/ft_sensor/wrench_calibrated',
                             '/left_arm/ft_sensor/set_zero'
                             )

        self._rarm.set_zero()
        self._larm.set_zero()

        self._joint_state = {}
        rospy.Subscriber('/joint_states', JointState, self.js_callback, queue_size=1)

        self.set_active_arm('rarm')
        self.state_dim = 18

        self._admc_active = False
        self._ik = IKSolver()

    def js_callback(self, msg):
        for n,p,v in zip(msg.name, msg.position, msg.velocity):
            self._joint_state[n] = [p, v]

    def set_active_arm(self, arm):
        if arm == 'rarm':
            self._arm = self._rarm
        elif arm == 'larm':
            self._arm = self._larm
        else:
            error('unknown arm: ', arm)

    def set_zero(self):
        self._rarm.set_zero()
        self._larm.set_zero()

    def get_state(self):
        """
        18 dim
        q1,...,q6, qv1,...,qv6, Fx,Fy,Fz,Tx,Ty,Tz
        """
        s = np.zeros(self.state_dim)

        try:
            js = [
                self._joint_state['rarm_shoulder_pan_joint'],
                self._joint_state['rarm_shoulder_lift_joint'],
                self._joint_state['rarm_elbow_joint'],
                self._joint_state['rarm_wrist_1_joint'],
                self._joint_state['rarm_wrist_2_joint'],
                self._joint_state['rarm_wrist_3_joint']
                ]
        except:
            return s

        for i in range(6):
            s[i] = js[i][0]
            s[6+i] = js[i][1]

        wrench = self._arm.state.wrench
        s[12] = wrench.force.x
        s[13] = wrench.force.x
        s[14] = wrench.force.x
        s[15] = wrench.torque.x
        s[16] = wrench.torque.x
        s[17] = wrench.torque.x
        return s

    def observe(self):
        s = self.get_state()
        obs = np.zeros(9)

        q = kdl.JntArray(6)
        qdot = kdl.JntArray(6)
        for i in range(6):
            q[i] = s[i][0]
            qdot[i] = s[6+i][0]
        qv = kdl.JntArrayVel(q, qdot)
        fv = kdl.FrameVel()
        self._ik.fksolvervel.JntToCart(qv, fv)

        frm = fv.GetFrame()
        base_frame = self._base_frame
        Tbase_world = t.inverse_matrix(pose2mat(base_frame))
        Tworld_wrist = np.zeros((4,4))
        for i in range(3):
            for j in range(3):
                Tworld_wrist[i,j] = frm.M[i,j]
        for i in range(3):
            Tworld_wrist[i,3] = frm.p[i]
        Tworld_wrist[3,3] = 1

        Ttcp_wrist = pose2mat((self._p0, self._q0))
        Tbase_tcp = np.dot(np.dot(Tbase_world, Tworld_wrist), t.inverse_matrix(Ttcp_wrist))

        for i in range(3):
            #obs[i] = p[i]
            obs[i] = Tbase_tcp[i,3]

        # v = fv.GetTwist().vel
        # for i in range(3):
        #     obs[3+i] = v[i]
        for i in range(6):
            obs[3+i] = s[12+i][0]
        return obs.reshape((1,-1))

    def control(self, u, time=1.0, wait=True):
        """
        u: [dx, dy, dz]
        """
        base_frame = self._base_frame

        Tworld_base = pose2mat(base_frame)
        u_world = np.dot(Tworld_base[:3,:3], u)
        print('PI_H[world] = %s' % u_world)

        #fv = np.average(self.get_state()[7:10,:], axis=1) # low-pass filter is better
        st = self.get_state()

        fv = st[12:15]
        fv2 = np.append(fv, 0)
        p_ft,q_ft = self.get_frame('rwrist_robotiq_ft_frame_id')
        fv_world = t.quaternion_multiply(t.quaternion_multiply(q_ft, fv2),
                                             t.quaternion_inverse(q_ft))[:3]

        force = np.linalg.norm(fv_world)
        print('FORCE(world) = %s, NORM = %s' % (fv_world, force))
        if force > 30:
            warn('STOP MOVING')
            return False

        # def unzip(l):
        #     return map(list, zip(*l))
        # pos,qv_cur = unzip(js)
        # qv_cur = np.array(qv_cur)

        pos = st[0:6]
        qv_cur = st[6:12]

        qp_cur = kdl.JntArray(6)
        qv_ref = kdl.JntArray(6)
        for j in range(6):
            qp_cur[j] = pos[j]

        Kf = 0.001
        vref = u_world + Kf*fv_world
        tw = kdl.Twist()
        tw.vel.x(vref[0])
        tw.vel.y(vref[1])
        tw.vel.z(vref[2])

        if self._ik.iksolvervel.CartToJnt(qp_cur, tw, qv_ref) != 0:
            error('IK failure')
            return False

        qv_ref2 = np.zeros(6)
        for j in range(6):
            qv_ref2[j] = qv_ref[j]

        dt = time
        Kv = 0.5
        qv_ref3 = qv_cur - Kv*qv_cur + qv_ref2
        dq = qv_ref3 * dt

        thre = 0.03
        if np.linalg.norm(dq) > thre:
            dp = thre / np.linalg.norm(dq) * dq
        # print('qv_cur, dq: %s, %s' % (qv_cur, dq))

        self._goal.trajectory.points = []
        point = JointTrajectoryPoint()
        point.velocities = qv_cur
        point.positions = pos
        point.time_from_start = rospy.Duration(-0.1)
        self._goal.trajectory.points.append(point)

        point = JointTrajectoryPoint()
        point.velocities = qv_ref3
        point.positions = pos + dq
        point.time_from_start = rospy.Duration(time)
        self._goal.trajectory.points.append(point)

        self._goal.trajectory.header.stamp = rospy.Time.now()
        self._traj_client.send_goal(self._goal)

        # if self._traj_client.get_state() == actionlib.GoalStatus.ACTIVE:
        #     self._traj_client.cancel_goal()
        if wait:
            self._traj_client.wait_for_result(timeout=rospy.Duration(time))

        return True
