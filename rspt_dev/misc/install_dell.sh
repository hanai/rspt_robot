#!/bin/bash

set -ue

# GLOBAL CONSTANTS
# ================
readonly UPDATE_APT="sudo apt-get update"
readonly INSTALL_APT="sudo apt-get install -y"
readonly INSTALL_PIP="sudo pip install --upgrade"
readonly GIT_CLONE="git clone --depth 1"

readonly PROGRAM_DIR=~/Program/
readonly ros_src=~/catkin_ws/src

# FUNCTIONS
# =========

function install_base_packages()
{
    $UPDATE_APT
    $INSTALL_APT \
	ssh build-essential cmake git unzip pkg-config libopencv-dev \
	python-pip libopencv-dev libgtk-3-dev python-numpy python-pytest \
	python3-numpy libdc1394-22 libdc1394-22-dev libjpeg-dev \
	libpng12-dev libtiff5-dev libjasper-dev libavcodec-dev \
	libavformat-dev libswscale-dev libxine2-dev libgstreamer1.0-dev \
	libgstreamer-plugins-base1.0-dev libv4l-dev libtbb-dev \
	qtbase5-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev \
	libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev \
	x264 v4l-utils vim emacs python-scipy python-matplotlib ccache tree xawtv
    $INSTALL_PIP numpy

    # pip install --upgrade pip

    return 0
}

function install_cuda()
{
    # checks https://developer.nvidia.com/cuda-downloads
    # local cuda_url=https://developer.nvidia.com/compute/cuda/8.0/Prod2/local_installers/cuda-repo-ubuntu1604-8-0-local-ga2_8.0.61-1_amd64-deb
    local cuda_url=http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.0.176-1_amd64.deb

    wget $cuda_url -O cuda.deb
    sudo dpkg -i cuda.deb
    sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
    $UPDATE_APT
    $INSTALL_APT cuda-9.0

    return 0
}

function install_cudnn()
{
    # get cudnn archive from https://developer.nvidia.com/cudnn
    local cudnn_pkg=libcudnn7_7.0.5.15-1+cuda9.0_amd64.deb
    local cudnn_dev_pkg=libcudnn7-dev_7.0.5.15-1+cuda9.0_amd64.deb

    sudo dpkg -i $cudnn_pkg
    sudo dpkg -i $cudnn_dev_pkg

    return 0
}

function install_tensorflow_and_keras()
{
    USE_GPU=$1
    if [[ $USE_GPU -eq 1 ]]; then
	( \
	    install_cuda && install_cudnn \
	)
    fi

    # $INSTALL_APT libcupti-dev # for CUDA Toolkit <= 7.5
    $INSTALL_APT cuda-command-line-tools-9.0
    echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/extras/CUPTI/lib64" >> ~/.bashrc
    $INSTALL_PIP tensorflow-gpu
    $INSTALL_PIP keras

    return 0
}

function install_ros()
{
    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
    $UPDATE_APT
    $INSTALL_APT python-rosinstall python-rosinstall-generator python-wstool build-essential
    $INSTALL_APT ros-kinetic-desktop-full && sudo rosdep init && rosdep update && \
	echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc && source ~/.bashrc && \
	mkdir -p ~/catkin_ws/src && cd  ~/catkin_ws/ && catkin_make && \
	echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc

    $INSTALL_APT ros-kinetic-moveit ros-kinetic-usb-cam
    return 0
}

# ============
# UR3/UR5/Sawyer,Robotiq etc.
# ============
function install_ur()
{
    $INSTALL_APT ros-kinetic-moveit ros-kinetic-usb-cam ros-kinetic-moveit-visual-tools
    $INSTALL_APT ros-kinetic-soem # for robotiq

    cd $ros_src
    $GIT_CLONE https://github.com/NihonBinary/universal_robot
    $GIT_CLONE https://github.com/NihonBinary/robotiq

    mkdir -p $PROGRAM_DIR && cd $PROGRAM_DIR
    $GIT_CLONE https://github.com/SintefRaufossManufactruing/python-urx && \
	cd python-urx && sudo python setup.py install
}

function install_sawyer()
{
    cd $ros_src
    $GIT_CLONE https://github.com/RethinkRobotics/sawyer_robot
    $GIT_CLONE https://github.com/RethinkRobotics/intera_sdk
    $GIT_CLONE https://github.com/RethinkRobotics/intera_common
    return 0
}

# ============
# BARRETT HAND
# ============
function install_usbcan_driver()
{
    local can_driver=peak-linux-driver-7.15.2
    $INSTALL_APT libpopt-dev
    cd $PROGRAM_DIR && \
	wget https://www.peak-system.com/fileadmin/media/linux/files/${can_driver}.tar.gz && \
	tar xfz ${can_driver}.tar.gz && cd ${can_driver} && \
	make clean && make NET=NO_NETDEV_SUPPORT && \ sudo make install && \
	sudo modeprobe pcan

    return 0
}

function install_pcan_python()
{
    $INSTALL_APT swig
    cd $PROGRAM_DIR && \
	$GIT_CLONE https://github.com/RobotnikAutomation/pcan_python && \
	cd pcan_python && make && sudo make install && \
	echo "export PYTHONPATH=/usr/lib:$PYTHONPATH" >> ~/.bashrc

    return 0
}

function patch_bhand()
{
    local robot_tool_dir=`rospack find robot_tool`
    local perception_palm_dir=`rospack find perception_palm`
    local rqt_bhand_dir=`rospack find rqt_bhand`

    cd $perception_palm_dir && patch -p1 < ${robot_tool_dir}/bhand/patch/perception_palm.patch
    cd $rqt_bhand_dir && patch -p2 < ${robot_tool_dir}/bhand/patch/rqt_bhand.patch

    return 0
}

function install_bhand_ros()
{
    $INSTALL_APT ros-kinetic-barrett-hand-description ros-kinetic-soem
    cd $ros_src 
    $GIT_CLONE https://github.com/RobotnikAutomation/Barrett_hand
    $GIT_CLONE https://github.com/RobotnikAutomation/barrett_hand_common
    $GIT_CLONE https://git.barrett.com/software/perception_palm

    patch_bhand

    wstool init . && \
	wstool merge perception_palm/perception_palm.rosinstall && \
	wstool update && cd ~/catkin_ws && source devel/setup.bash && \
	catkin_make

    return 0
}

function install_barrett_hand()
{
    install_usbcan_driver
    install_pcan_python
    install_bhand_ros

    return 0
}

# ============
# CHOREONOID
# ============
function install_choreonoid()
{
    local CHOREONOID=choreonoid-1.6.0
    cd $PROGRAM_DIR && \
	wget http://choreonoid.org/_downloads/$CHOREONOID.zip && unzip $CHOREONOID.zip && \
	ln -s $CHOREONOID choreonoid && choreonoid/misc/script/install-requisites-ubuntu-16.04.sh && build_choreonoid

    return 0
}

function build_choreonoid()
{
    local build_dir=$PROGRAM_DIR/choreonoid/build
    mkdir -p $build_dir && cd $build_dir && \
	cmake .. -DBUILD_ASSIMP_PLUGIN=OFF -DBUILD_BALANCER_PLUGIN=OFF -DBUILD_PYTHON_SIM_SCRIPT_PLUGIN=OFF -DCMAKE_BUILD_TYPE=Debug -DENABLE_SAMPLES=OFF -DINSTALL_SDK=OFF && make -j8

    return 0
}

# ============
# TEACHINGPLUGIN
# ============
function install_teachingplugin()
{
    curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash && \
	$INSTALL_APT git-lfs && git lfs install
    $INSTALL_APT libqt5sql5-sqlite libsqlite3-dev sqlite3

    local ext_dir=$PROGRAM_DIR/choreonoid/ext
    $GIT_CLONE -b dev17 https://bitbucket.org/hanai/teachingplugin $ext_dir/teachingPlugin
    $GIT_CLONE https://bitbucket.org/hanai/samplehirocontroller2plugin $ext_dir/SampleHiroController2Plugin
}

function initial_setup()
{
    LANG=C xdg-user-dirs-gtk-update

    # $INSTALL_APT gnome-tweak-tool # exchange Caps & Ctrl

    # get .mybashrc && echo "source ~/.mybashrc" >> ~/.bashrc
    # get .vimrc, .vim .emacs etc.

    return 0
}


function setup()
{
    if [ $1 = base ]; then
	install_base_packages
    elif [ $1 = ros ]; then
	install_ros
    elif [ $1 = tensorflow ]; then
	install_tensorflow_and_keras 0 # 0: not use gpu, 1: use gpu
    elif [ $1 = robot ]; then
	install_robot_packages
    elif [ $1 = choreonoid ]; then
	install_choreonoid
    elif [ $1 = teachingplugin ]; then
	install_teachingplugin
    elif [ $1 = barret ]; then
	install_barrett_hand
    elif [ $1 = ur ]; then
	install_ur
    elif [ $1 = sawyer ]; then
	install_sawyer
    else
	echo "unknown flag: $1"
    fi

    return 0
}

for x in "$@"
do
    setup $x
done

exit 0

