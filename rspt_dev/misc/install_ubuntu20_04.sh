#!/bin/bash

set -ue

# GLOBAL CONSTANTS
# ================
readonly UPDATE_APT="sudo apt update"
readonly INSTALL_APT="sudo apt install -y"
readonly INSTALL_PIP="sudo pip3 install --upgrade"
#readonly GIT_CLONE="git clone --depth 1"
readonly GIT_CLONE="git clone"

readonly PROGRAM_DIR=~/Program/
readonly catkin_ws=~/catkin_ws
readonly ros_src=~/catkin_ws/src

# FUNCTIONS
# =========

function install_base_packages()
{
    $UPDATE_APT
    $INSTALL_APT \
	ssh build-essential cmake git unzip pkg-config libopencv-dev \
	python3-pip \
	libjpeg-dev libpng-dev libtiff-dev libavcodec-dev \
	libavformat-dev libswscale-dev libxine2-dev libgstreamer1.0-dev \
	libgstreamer-plugins-base1.0-dev libv4l-dev libtbb-dev \
	qtbase5-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev \
	libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev \
	x264 v4l-utils vim emacs ccache tree xawtv
    $INSTALL_PIP numpy scipy pytest matplotlib ipython

    # pip install --upgrade pip

    return 0
}

# function install_cuda()
# {
#     # checks https://developer.nvidia.com/cuda-downloads
#     # local cuda_url=https://developer.nvidia.com/compute/cuda/8.0/Prod2/local_installers/cuda-repo-ubuntu1604-8-0-local-ga2_8.0.61-1_amd64-deb
#     local cuda_url=http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.0.176-1_amd64.deb

#     wget $cuda_url -O cuda.deb
#     sudo dpkg -i cuda.deb
#     sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
#     $UPDATE_APT
#     $INSTALL_APT cuda-9.0

#     return 0
# }

function install_cuda()
{
    wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
    sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
    sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub
    sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
    $UPDATE_APT
    $INSTALL_APT cuda
}


# function install_cudnn()
# {
#     # get cudnn archive from https://developer.nvidia.com/cudnn
#     local cudnn_pkg=libcudnn7_7.0.5.15-1+cuda9.0_amd64.deb
#     local cudnn_dev_pkg=libcudnn7-dev_7.0.5.15-1+cuda9.0_amd64.deb

#     sudo dpkg -i $cudnn_pkg
#     sudo dpkg -i $cudnn_dev_pkg

#     return 0
# }

# function install_tensorflow_and_keras()
# {
#     USE_GPU=$1
#     if [[ $USE_GPU -eq 1 ]]; then
# 	( \
# 	    install_cuda && install_cudnn \
# 	)
#     fi

#     # $INSTALL_APT libcupti-dev # for CUDA Toolkit <= 7.5
#     $INSTALL_APT cuda-command-line-tools-9.0
#     echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/extras/CUPTI/lib64" >> ~/.bashrc
#     $INSTALL_PIP tensorflow-gpu
#     $INSTALL_PIP keras

#     return 0
# }

function install_pytorch()
{
    USE_GPU=$1
    if [[ $USE_GPU -eq 1 ]]; then
	echo 'Install with GPU'
	install_cuda
	$INSTALL_PIP torch==1.8.1+cu111 torchvision==0.9.1+cu111 torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
    else
	echo 'Install without GPU'
	$INSTALL_PIP torch==1.8.1+cpu torchvision==0.9.1+cpu torchaudio==0.8.1 -f https://download.pytorch.org/whl/torch_stable.html
    fi
    
    return 0
}

function install_ros()
{
    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
    
    $UPDATE_APT
    $INSTALL_APT python3-rosdep python3-rosinstall python3-rosinstall-generator \
		 python3-wstool build-essential
    $INSTALL_APT ros-noetic-desktop-full && sudo rosdep init && rosdep update && \
	echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc && source ~/.bashrc && \
	mkdir -p ~/catkin_ws/src && cd  ~/catkin_ws/ && catkin_make && \
	echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc

    $INSTALL_APT ros-noetic-moveit ros-noetic-usb-cam ros-noetic-moveit-visual-tools
    return 0
}

# ============
# UR3/UR5/Sawyer,Robotiq etc.
# ============
function install_ur()
{
    cd $catkin_ws

    # clone the driver
    $GIT_CLONE https://github.com/UniversalRobots/Universal_Robots_ROS_Driver.git src/Universal_Robots_ROS_Driver

    # clone fork of the description. This is currently necessary, until the changes are merged upstream.
    $GIT_CLONE -b calibration_devel https://github.com/fmauch/universal_robot.git src/fmauch_universal_robot

    # install dependencies
    $UPDATE_APT -qq
    rosdep update && rosdep install --from-paths src --ignore-src -y

    # build the workspace. We need an isolated build because of the non-catkin library package.
    catkin_make
    # activate the workspace (ie: source it)
    source devel/setup.bash
}

function install_robotiq()
{
    $INSTALL_APT ros-kinetic-soem # for robotiq
    $GIT_CLONE https://github.com/NihonBinary/universal_robot
    $GIT_CLONE https://github.com/NihonBinary/robotiq

    mkdir -p $PROGRAM_DIR && cd $PROGRAM_DIR
    $GIT_CLONE https://github.com/SintefRaufossManufactruing/python-urx && \
	cd python-urx && sudo python setup.py install
}

function install_sawyer()
{
    cd $ros_src
    $GIT_CLONE https://github.com/RethinkRobotics/sawyer_robot
    $GIT_CLONE https://github.com/RethinkRobotics/intera_sdk
    $GIT_CLONE https://github.com/RethinkRobotics/intera_common
    return 0
}

# ============
# BARRETT HAND
# ============
function install_usbcan_driver()
{
    local can_driver=peak-linux-driver-7.15.2
    $INSTALL_APT libpopt-dev
    cd $PROGRAM_DIR && \
	wget https://www.peak-system.com/fileadmin/media/linux/files/${can_driver}.tar.gz && \
	tar xfz ${can_driver}.tar.gz && cd ${can_driver} && \
	make clean && make NET=NO_NETDEV_SUPPORT && \ sudo make install && \
	sudo modeprobe pcan

    return 0
}

function install_pcan_python()
{
    $INSTALL_APT swig
    cd $PROGRAM_DIR && \
	$GIT_CLONE https://github.com/RobotnikAutomation/pcan_python && \
	cd pcan_python && make && sudo make install && \
	echo "export PYTHONPATH=/usr/lib:$PYTHONPATH" >> ~/.bashrc

    return 0
}

function patch_bhand()
{
    local robot_tool_dir=`rospack find robot_tool`
    local perception_palm_dir=`rospack find perception_palm`
    local rqt_bhand_dir=`rospack find rqt_bhand`

    cd $perception_palm_dir && patch -p1 < ${robot_tool_dir}/bhand/patch/perception_palm.patch
    cd $rqt_bhand_dir && patch -p2 < ${robot_tool_dir}/bhand/patch/rqt_bhand.patch

    return 0
}

function install_bhand_ros()
{
    $INSTALL_APT ros-kinetic-barrett-hand-description ros-kinetic-soem
    cd $ros_src 
    $GIT_CLONE https://github.com/RobotnikAutomation/Barrett_hand
    $GIT_CLONE https://github.com/RobotnikAutomation/barrett_hand_common
    $GIT_CLONE https://git.barrett.com/software/perception_palm

    patch_bhand

    wstool init . && \
	wstool merge perception_palm/perception_palm.rosinstall && \
	wstool update && cd ~/catkin_ws && source devel/setup.bash && \
	catkin_make

    return 0
}

function install_barrett_hand()
{
    install_usbcan_driver
    install_pcan_python
    install_bhand_ros

    return 0
}

# ============
# CHOREONOID
# ============
function install_choreonoid()
{
    local CHOREONOID=choreonoid-1.7.0
    cd $PROGRAM_DIR && \
	wget http://choreonoid.org/_downloads/82c0099aa3492a273519e1906ea56e54/$CHOREONOID.zip && unzip $CHOREONOID.zip && \
	ln -s $CHOREONOID choreonoid && choreonoid/misc/script/install-requisites-ubuntu-18.04.sh && build_choreonoid

    return 0
}

function build_choreonoid()
{
    local build_dir=$PROGRAM_DIR/choreonoid/build
    mkdir -p $build_dir && cd $build_dir && \
	cmake .. -DBUILD_ASSIMP_PLUGIN=OFF -DBUILD_BALANCER_PLUGIN=OFF -DBUILD_PYTHON_SIM_SCRIPT_PLUGIN=OFF -DCMAKE_BUILD_TYPE=Debug -DENABLE_SAMPLES=OFF -DINSTALL_SDK=OFF && make -j8

    return 0
}

# ============
# TEACHINGPLUGIN
# ============
function install_teachingplugin()
{
    local choreonoid_dir=$PROGRAM_DIR/choreonoid
        
    $GIT_CLONE -b dev19 https://github.com/ryhanai/teachingplugin $choreonoid_dir/ext/teachingPlugin
    $GIT_CLONE -b dev19 https://github.com/ryhanai/SampleTPControllerPlugin $choreonoid_dir/ext/SampleTPControllerPlugin
    $choreonoid_dir/ext/teachingPlugin/misc/script/install-requisites-ubuntu-18.04.sh
    cd $choreonoid_dir/build && cmake .. -DBUILD_TEACHING_PLUGIN=ON -DBUILD_SAMPLE_HIROCONTROLLER=ON -DUSE_ROS=OFF && make -j4
}

function initial_setup()
{
    LANG=C xdg-user-dirs-gtk-update

    # $INSTALL_APT gnome-tweak-tool # exchange Caps & Ctrl

    # get .mybashrc && echo "source ~/.mybashrc" >> ~/.bashrc
    # get .vimrc, .vim .emacs etc.

    return 0
}


function setup()
{
    if [ $1 = base ]; then
	install_base_packages
    elif [ $1 = ros ]; then
	install_ros
    elif [ $1 = tensorflow ]; then
	install_tensorflow_and_keras 0 # 0: not use gpu, 1: use gpu
    elif [ $1 = pytorch ]; then
	install_pytorch 0 # 0: not use gpu, 1: use gpu
    elif [ $1 = robot ]; then
	install_robot_packages
    elif [ $1 = choreonoid ]; then
	install_choreonoid
    elif [ $1 = teachingplugin ]; then
	install_teachingplugin
    elif [ $1 = barret ]; then
	install_barrett_hand
    elif [ $1 = ur ]; then
	install_ur
    elif [ $1 = sawyer ]; then
	install_sawyer
    elif [ $1 = cuda ]; then
	install_cuda
    else
	echo "unknown flag: $1"
    fi

    return 0
}

for x in "$@"
do
    setup $x
done

exit 0

