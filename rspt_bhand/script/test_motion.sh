#!/bin/bash

function test()
{
    # initialization motion
    rosservice call /bhand_node/actions 'action: 1' 
    sleep 2

    # CLOSE
    rosservice call /bhand_node/actions 'action: 2'
    sleep 2

    # OPEN
    rosservice call /bhand_node/actions 'action: 3'
    sleep 2

    #
    rosservice call /bhand_node/set_control_mode 'mode: POSITION'
    rostopic pub /bhand_node/command sensor_msgs/JointState "header:
  seq: 0
  stamp: {secs: 0, nsecs: 0}
  frame_id: ''
name: ['bh_j11_joint', 'bh_j32_joint', 'bh_j12_joint', 'bh_j22_joint']
position: [0.0, 0, 0, 0]
velocity: [0, 0, 0, 0]
effort: [0, 0, 0, 0]"

    return 0
}

test

exit 0



# http://wiki.ros.org/bhand_controller
#
# /joint_states
# position, velocity, effort
# fingertip torque sensor values are published in effort
