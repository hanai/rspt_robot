#!/usr/bin/python3
# encoding: utf-8

from arcllib import *

client = telnetlib.Telnet(host="192.168.11.99", port=7171)
client.write(b'admin\n')
client.read_until(b'End of commands')
client.read_until(b'\n')

def status():
    """
    status()
    """
    client.write(b'status\n')
    client.read_until(b'StateOfCharge: ')
    charge = client.read_until(b'\n')
    client.read_until(b'Location: ')
    location = client.read_until(b'\n')
    client.read_until(b'LocalizationScore: ')
    localization_score = client.read_until(b'\n')
    client.read_until(b'Temperature: ')
    temperature = client.read_until(b'\n')
    return [charge, location, localization_score, temperature]

def getGoals():
    """
    getGoals()
    """
    client.write(b'getGoals\n')
    goalstr = client.read_until(b'End of goals')
    goals = goalstr.split()[1:-3:2]
    client.read_until(b'\n')
    return goals

def goto(goal):
    """
    goto('g1')
    """
    client.write(b'goto '+goal.encode()+b'\n')
    print(client.read_until(b'\n'))
    print(client.read_until(b'\n'))

def wait(sec):
    """
    wait(10)
    """
    client.write(b'doTask wait '+str(sec).encode()+b'\n')
    client.read_until(b'Completed doing task')
    client.read_until(b'\n')

def say(message):
    """
    say('hello')
    """
    client.write(b'say '+message.encode()+b'\n')
    client.read_until(b'Saying')
    client.read_until(b'\n')
