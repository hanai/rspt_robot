# encoding: utf-8

import telnetlib

class TelnetClient:
    """

    """

    def __init__(self, host='localost', port=23, user='artuser', password='artuser'):
        self.host = host
        self.port = port
        self.user = user
        self.password = password

    def __del__(self):
        self.disconnect()

    def connect(self):
        self.tn = telnetlib.Telnet(host=self.host, port=self.port)
        self.tn.read_until(b"login: ")
        self.tn.write(self.user.encode('ascii') + b"\n")
        if self.password:
            self.tn.read_until(b"Password: ")
            self.tn.write(self.password.encode('ascii') + b"\n")

    def disconnect(self):
        self.tn.write(b"exit\n")
        #self.tn.close()

    def sendCommand(self, cmd, print_command=False, print_response=False):
        """
        """
        self.connect()
        if print_command:
            print(cmd)
        self.tn.write(b"%s\n" % cmd.encode())
        self.disconnect()
        response = self.tn.read_all().decode('utf8')
        if print_response:
            print(response)
        return response

    def do_something(self):
        self.sendCommand("ls")


class ARCLClient(TelnetClient):
    """
    sendCommandのエラー処理は実装されていません（誰か実装して）

    エラーパターン（ARCLリファレンスマニュアルp46）
    Unknown command: 存在しないコマンド
    SetUpError:
    CommandError, CommandErrorDescription

    コマンド実行
    Doing task <task> <argument>
    ...
    Completed doing task <task> <argument>

    """

    def wait(self, second):
        response = self.sendCommand('doTask wait %d' % second)
        # parse the commen response, which depends on each command.
        result = True
        return result

    def goto(self, goal):
        """
        地図上に設定したgoalへ移動させる。

        Parameters
        ----------
        goal : string
            地図に定義されたgoal名。

        Returns
        ----------
        result : bool
            goalに到達したらtrue。到達できなければfalse。
        """

        response = self.sendCommand('doTask go %s' % goal)
        result = True
        return result

    def say(self, message):
        """
        """
        response = self.sendCommand('doTask say "%s"' % message)
        return True

    def move(self):
        """
        """
        #response = self.sendCommand('doTask move')
        return True

    def deltaHeading(self):
        """
        """
        response = self.sendCommand('doTask deltaHeading')
        return True

    def setHeading(self):
        """
        """
        response = self.sendCommand('doTask setHeading')
        return True

    def triangleDriveTo(self):
        """
        """
        response = self.sendCommand('doTask triangleDriveTo')
        return True

    def trianglePointAway(self):
        """
        """
        response = self.sendCommand('doTask trianglePointAway')
        return True

    def cartCapture(self):
        """
        """
        response = self.sendCommand('doTask cartCapture')
        return True

    def cartRelease(self):
        """
        """
        response = self.sendCommand('doTask cartRelease')
        return True

    def followGuide(self):
        """
        """
        response = self.sendCommand('doTask followGuide')
        return True

    def getGoals(self):
        """
        """
        response = self.sendCommand('getGoals', show_response=True)
        return True

